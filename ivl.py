import sys, ntpath, os
from PyQt4 import QtCore, QtGui, uic
from subprocess import call

ivpath = "C:\\iverilog\\bin\\"
form_class = uic.loadUiType("ivloader.ui")[0] 

class MyClass(QtGui.QMainWindow, form_class):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def dbg_pr_str(self, kl):
        dstr = ""
        for k in kl:
            dstr += k +" "
        
        self.status_txt.append(dstr)

    def select(self):
        self.dbg_pr_str(["Currently in", os.getcwd()])
        self.vfile = QtGui.QFileDialog.getOpenFileName(self, 'Open testbench', '.')
        self.vfiledir = ntpath.dirname(str(self.vfile))
        self.vfilename = ntpath.basename(str(self.vfile)).split('.')[-2]
        self.dbg_pr_str(["Selected "+self.vfile])
        os.chdir(self.vfiledir)
        self.dbg_pr_str(["Changed to folder", os.getcwd()])

    def compile(self):
        self.compiled_filename = self.vfiledir+"/icarus_compile/000_"+self.vfilename+".compiled"
#        self.compiled_filename = self.vfiledir+"/000_"+self.vfilename+".compiled"
        ivargs = [ivpath+"iverilog",
                  self.vfile,
                  "-I",
                  self.vfiledir,
                  "-o",
                  self.compiled_filename]
        call(ivargs)
        self.dbg_pr_str(ivargs)
        
    def simulate(self):
        vvargs = [ivpath+"vvp",
                  self.compiled_filename,
                  "-lxt2"]
        call(vvargs)
        self.dbg_pr_str(vvargs)

    def open_gtkwave(self):
        gtklxt = self.vfiledir+"/icarus_compile/000_"+self.vfilename+".lxt"
        gtksave = self.vfiledir+"/icarus_compile/001_"+self.vfilename+".sav"
        #gtklxt = self.vfiledir+"/000_"+self.vfilename+".lxt"
        #gtksave = self.vfiledir+"/001_"+self.vfilename+".sav"
        gtkargs = [ivpath+"gtkwave",
                   gtklxt,
                   gtksave]
        call(gtkargs)
        self.dbg_pr_str(gtkargs)
        

app = QtGui.QApplication(sys.argv)
mywindow = MyClass(None)

#import pdb;pdb.set_trace()
mywindow.show()
app.exec_()

